# Grandfather-father-son Drush backup script #

Simple script based on [Grandfather-father-son](https://en.wikipedia.org/wiki/Backup_rotation_scheme#Grandfather-father-son) backup schema.

Before use, edit file and change beginning settings section save it and run it every day. You want probably put this in your `crontab` with following settings: 

`00 00 * * * /path/to/your/gfs-backup.sh`

That's it..