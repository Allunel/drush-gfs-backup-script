#!/bin/bash
################################
#
# Backup script using drush with
# Grandfather-Father-Son schema.
#
################################



############ SETTINGS

# Drupal app root
DRUPAL_ROOT=/path/to/your/drupal/app

# Where to backup to.
DEST=~/backup

# Setup variables for the archive filename.
# Week day.
DAY=$(date +%A)


HOSTNAME=$(hostname -s)

# Find which week of the month 1-4 it is.
DAY_NUM=$(date +%d)
######################## END OF SETTINGS





######################### SCRIPT
if (( $DAY_NUM <= 7 )); then
  WEEK_FILE="$HOSTNAME"'_week1'
elif (( $DAY_NUM > 7 && $DAY_NUM <= 14 )); then
  WEEK_FILE="$HOSTNAME"'_week2'
elif (( $DAY_NUM > 14 && $DAY_NUM <= 21 )); then
  WEEK_FILE="$HOSTNAME"'_week3'
elif (( $DAY_NUM > 21 && $DAY_NUM < 32 )); then
  WEEK_FILE="$HOSTNAME"'_week4'
fi

# Find if the Month is odd or even.
MONTH_NUM=$(date +%m)
MONTH=$(expr $MONTH_NUM % 2)
if [ $MONTH -eq 0 ]; then
  MONTH_FILE="$HOSTNAME"'_month-even'
else
  MONTH_FILE="$HOSTNAME"'_month-odd'
fi


##########################
# Main decition part of backup!
#
# Is it monthly, weekly or daily backup???
#
if [ $DAY_NUM == 1 ]; then
  ARCHIVE_FILE=$MONTH_FILE
elif [ $DAY != "Saturday" ]; then
  ARCHIVE_FILE="$HOSTNAME-$DAY"
else
  ARCHIVE_FILE=$WEEK_FILE
fi

# Print start status message.
echo " > Backing up $PROJECT_NAME at: `date`"
echo

# Backup database!
echo -n ' > '
drush --root=$DRUPAL_ROOT sql-dump \
      --gzip \
      --skip-tables-key=common \
      --result-file=$DEST/$ARCHIVE_FILE


# Print end status message.
echo
echo " > Backup finished at: `date`"

# Long listing of files in $DEST to check file sizes.
echo
echo ' > Output of backup directory:'
ls -lh $DEST | sed "s/$ARCHIVE_FILE\.gz$/& <<< This backup was made now\!/"
